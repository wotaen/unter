<!doctype html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <title>Vojta Untermüller &ndash; Digital copywriter and idea maker</title>
    <meta name="description" content="Digital copywriter and idea maker.">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="author" content="Michal Kotulek, Michal Holub">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
    <link rel="stylesheet" media="all" href="files/style.css?v=1">
    <!--[if lt ie 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <div class="wrapper">
        <header>
            <h1>Vojta Untermüller</h1>
            <h3>Digital copywriter and idea maker</h3>
        </header>
        <section class="about">
            <div class="page">
                <h2><b><span>Hello,</span></b><br><span>my name is Vojta</span></h2>
            </div>
            <div class="page">
                <h3><span>I </span><span>am </span><span>digital </span><span>copywriter</span><br><span> and </span><span>idea maker</span><span>.</span></h3>
                <h4><span>I like challenges</span><span> of</span><br><span>digital advertising and online marketing.</span></h4>
            </div>
        </section>
        <section class="references">
            <div class="page">
                <div class="slide">
                    <h3><a href="#references" class="active" title="References"><span>References</span></a><span class="slash">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&frasl;&nbsp;&nbsp;&nbsp;&nbsp;</span><a href="#clients" title="Clients"><span>Clients</span></a><span class="slash">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&frasl;&nbsp;&nbsp;&nbsp;&nbsp;</span><a href="#news" title="News"><span>News</span></a></h3>
                    <div class="carousel">
                        <div class="item">
                            <dl class="references">
                                <dt class="active"><a href="index.html#" title="Peter Podolinský">Peter Podolinský</a></dt>
                                <dd class="active"><strong>&frasl;&nbsp;&nbsp;&nbsp;Client Partner, Czech &frasl; Facebook, Ireland</strong><br>I used to work with Vojta in an ad agency as well as from my post of socila media consultant. Vojta is a hard worker, there is nothing he leaves undone. Before he stars working, he first studies all possible sources to create himslef the adequate room for further creative thinking. His ideas are relevant, insight-full and presented with passion. He is very well acquainted with czech mentality and able to tune himself up to his target audience. His work for Vodafone and other clients proofs that he simply „can do it“. If looking for someone who creates unique and original (social) content I can´t but recommend him.</dd>
                                <dt><a href="index.html#" title="Michal Babka">Michal Babka</a></dt>
                                <dd><strong>&frasl;&nbsp;&nbsp;&nbsp;Creative Director (Dílna, MOTM, BTL MCE, Remmark)</strong><br>Best copywriters are smart, lettered, curious. Like Vojta. If you motivate him properly, you got a perfect copywriter and idea maker. But he has to be fully dragged into the course of agency processes – as any quality copywriter. Vojta is excellent in creative concepts. Uses mindmaps. Papers are everywhere. Also tends to be socially responsible. And is quite good at drawing, also :)</dd>
                                <dt><a href="index.html#" title="Martina Loutná">Martina Loutná</a></dt>
                                <dd><strong>&frasl;&nbsp;&nbsp;&nbsp;Ředitelka Dobrá rodina o.p.s.</strong><br>The goal of our project of Dobrá rodina („The Goog Family“) is to change the situation of foster care in CZ and Vojta has been supporting our organisation with his gratutious and highly professional activities, when creating first marketing materials, concepts and strategies. He inspires, goes deep, even suggests graphic designs and is very good at networking. Though he is a man of clear and solid opinions, he i also able to find compromises and solutions that, in the end, fit to everyone.</dd>
                                <dt><a href="index.html#" title="Marcel Jinoch">Marcel Jinoch</a></dt>
                                <dd><strong>&frasl;&nbsp;&nbsp;&nbsp;Financial Consultant at Swiss Life Select</strong><br>Vojta is a professional. It is a pleasure to work with him. Vojta brings new perspectives and ideas in projects he participates in. Always does his job on 110 % and even inspires others to better performance. Everyone enjoys brainstorming with him.</dd>
                                <dt><a href="index.html#" title="Lars Killi">Lars Killi</a></dt>
                                <dd><strong>&frasl;&nbsp;&nbsp;&nbsp;Chief Creative Officer at McCann Worldgroup</strong><br>Anyone that would be looking for a sharp pen, is probably looking for someone like Vojta. I have seen him take the steps from a copywriter covering the tasks of traditional advertising to, in the last couple of years, making his mark in the field of digital copywriting, with all the extra insight this requires. On this background I can safely say that he is a contemporary writer that masters the tasks that modern communication demands.</dd>
                                <dt><a href="index.html#" title="Dana Šimíčková">Dana Šimíčková</a></dt>
                                <dd><strong>&frasl;&nbsp;&nbsp;&nbsp;Account Manager ve společnosti TRIAD Advertising s.r.o.</strong><br>I wish all my co-workers were as nice as Vojta. He is also a true gentleman which makes him a good companion. Creative, funny, frank, straightforward, passionate. If you are looking for a bright creative person with a good taste - don´t hesitate. It´s him.</dd>
                            </dl>
                        </div>
                        <div class="item">
                            <dl class="clients">
                                <dt>Current:</dt>
                                <a href="#" target="_blank"></a>,
                                <dd>
                                    <a href="http://www.digitalvision.cz/reference/ceska-pojistovna-ref/" target="_blank">Česká pojištovna</a>,
                                    <a href="http://www.csas.cz/banka/appmanager/portal/konsolidace?_nfpb=true&_pageLabel=konsolidace&from=kons_menu&docid=internet/cs/sc_13535.xml" target="_blank">Česká spořitelna</a>,
                                    <a href="http://www.digitalvision.cz/reference/lego1/" target="_blank">LEGO</a>,
                                    Tesco (<a href="http://nakup.itesco.cz/cs-CZ/" target="_blank">Itesco</a>, <a href="http://www.clubcard.cz/" target="_blank">Clubcard</a>),
                                    <a href="http://www.koncertpro4kola.cz/" target="_blank">Subaru</a>,
                                    Kia (<a href="http://select.kia.com/cz/" target="_blank">Kia.cz</a>, <a href="http://akce.kia.com/cz/" target="_blank">Akce</a>, <a href="http://www.kia.com/cz/modely/kia-ceed/?utm_source=adwords_vyhledavani&utm_medium=text&utm_term=defend&utm_campaign=2014" target="_blank">Ceed</a>),
                                    Mediafactory/Digital Vision PF 2014 (<a href="http://www.mediafactory.cz/pf2014/" target="_blank">PF 2014</a>)
                                </dd>
                                <dt>Past:</dt>
                                <dd>
                                    Aegon (<a href="https://www.youtube.com/watch?v=-QoTk8md3xY" target="_blank">#1</a>, <a href="https://www.youtube.com/watch?v=HDJ2pEPfRFw" target="_blank">#2</a>),
                                    Blesk (<a href="https://www.youtube.com/watch?v=O0Yyfhm7E88" target="_blank">#1</a>, <a href="https://www.youtube.com/watch?v=9Kux65q2eY8&feature=related" target="_blank">#2</a>)
                                    <a href="https://www.youtube.com/watch?v=pYR4YDJh88Y" target="_blank">Budwesier Budvar</a>,
                                    <a href="http://www.galeriesantovka.cz/" target="_blank">Galerie Šantovka</a>,
                                    <a href="http://www.galerieteplice.cz/" target="_blank">Galerie Teplice</a>,
                                    <a href="https://www.youtube.com/watch?v=Pn9i9IayqeE" target="_blank">Lidice Memorial (online)</a>,
                                    <a href="https://www.youtube.com/watch?v=1s2qdjjN7A8" target="_blank">U:fon</a>,
                                    <a href="https://www.youtube.com/watch?v=lIoDScnE2m4" target="_blank">Vodafone</a>,
                                    <a href="https://www.facebook.com/vodafoneCZ?fref=ts" target="_blank">Vodafone Facebook (until 1. 8. 2013)</a>,
                                    See <a href="http://www.tumblr.com/blog/vojtechuntermuller" target="_blank">other prints, tv ads and radio spots</a> (in CZ)
                                    <br/>
                                    + Raiffeisenbank, Opel, Microsoft, Kofola, Nestlé, Heineken (Krušovice) Oskar, Eurotel, Česká pošta, Procter&Gamble, MasterCard, BMW Mini, Unicredit bank, T-Mobile, Prima TV, L´Oreal, České dráhy, Volvo, GSK, Harley´s bar Prague and probably nothing else.
                                </dd>

                            </dl>
                        </div>
                        <div class="item">
                            <dl class="news">
                                <?php include ('news.php') ?>
                            </dl>
                        </div>
                    </div>
                    <ul>
                        <li>See my CV and recomendations on <a href="http://www.linkedin.com/in/vojtechuntermuller" title="LinkedIn" rel="external">LinkedIn</a></li>
                        <li>Phone: +420 777 144 727</li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="contact">
            <div class="page">
                <div class="form">
                    <h3><span>Want to keep your </span><span>business</span><span> </span><span>growing</span><span>?</span><br><span>Contact me</span><span>:</span></h3>
                    <form method="post" action="send.php" id="contact-form">
                        <input type="text" name="name" placeholder="Name">
                        <input type="email" name="email" placeholder="E-mail"><br>
                        <div class="wrap">
                        <textarea name="message" placeholder="Message"></textarea>
                        <img src="files/vojta-foto.jpg" align="right" height="142" class="photo"/>
                        </div>
                        <div class="clear"></div>
                        <input type="submit" value="Send"/>

                        <input type="text" name="email-confirm" value="" style="display: none">
                    </form>
                    <div id="circleG" class="hidden">
                        <div id="circleG_1" class="circleG">
                        </div>
                        <div id="circleG_2" class="circleG">
                        </div>
                        <div id="circleG_3" class="circleG">
                        </div>
                    </div>
                    <div id="message" class="hidden">
                        <h4>Hi there, thanks for the message, will get in touch.</h4>
                    </div>

                </div>
            </div>
            <footer>
                <ul>
                    <li>Vojta Untermüller: </li>
                    <li><a href="https://www.facebook.com/vojta.unter" rel="external" title="Facebook">Facebook</a></li>
                    <li><a href="http://www.linkedin.com/in/vojtechuntermuller" rel="external" title="LinkedIn">LinkedIn</a></li>
                    <li>Phone: +420 777 144 727</li>
                </ul>
            </footer>
        </section>
    </div>
    <nav>
        <ul>
            <li><a href="/" title="About">About</a>
                <ul>
                    <li class="breadcrumb"><a href="/" title="About">&#xe601;</a></li>
                    <li class="breadcrumb"><a href="#about" title="About">&#xe600;</a></li>
                </ul>
            </li>
            <li><a href="#references" title="Work">Work</a>
                <ul class="work">
                    <li class="breadcrumb"><a href="#references" title="References">&#xe600;</a></li>
                    <li class="breadcrumb"><a href="#clients" title="Clients">&#xe600;</a></li>
                    <li class="breadcrumb"><a href="#news" title="News">&#xe600;</a></li>
                </ul>
            </li>
            <li><a href="#contact" title="Contact">Contact</a>
                <ul>
                    <li class="breadcrumb"><a href="#contact" title="Contact">&#xe600;</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
    <script>window.jQuery || document.write('<script src="files/jquery.js"><\/script>')</script>
    <!--<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>-->
    <script>window.jQuery.ui || document.write('<script src="files/jquery-ui.js"><\/script>')</script>
    <!--[if (gte ie 6)&(lte ie 8)]><script src="files/selectivizr.js"></script><![endif]-->
    <script src="files/jquery.touchSwipe.min.js"></script>
    <script src="files/mousewheel.js"></script>
    <script src="files/scripts.js"></script>

<div id="r"></div>
</body>
</html>