<?php

spl_autoload_register(function($class){
        require preg_replace('{\\\\|_(?!.*\\\\)}', DIRECTORY_SEPARATOR, ltrim($class, '\\')).'.php';
    });

use Michelf\Markdown;

$news = file_get_contents('news.txt');
$parsedNews = array();

foreach(explode('-----',$news) as $index => $new) {
    $items = explode("\n",trim($new));
    $body = implode("<br/>",array_slice($items,3));

    $parsedNews[] = array(
        'date' => $items[0],
        'headline' => $items[1],
        'body'      => Markdown::defaultTransform($body),
        'id' => $index
    );
}

?>


<ul class="dates">
    <?php foreach($parsedNews as $new): ?>
        <li>
            <a href="#new-<?php echo $new['id']; ?>">
                <?php echo $new['date']; ?>
            </a>
        </li>
    <?php endforeach; ?>
</ul>


<?php foreach($parsedNews as $new): ?>
    <div id="new-<?php echo $new['id']; ?>" class="new <?php if ($new['id']>0):?>hidden<?php endif; ?>">
        <h4><?php echo $new['headline']; ?></h4>
        <?php echo $new['body']; ?>
    </div>
<?php endforeach; ?>