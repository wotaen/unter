var mql = null;

var STEPS = {
    0: "#home",
    1: "#about",
    2: "#references",
    3: "#clients",
    4: "#news",
    5: "#contact"
}

function isWork(step) {
    return step == 2 || step == 3 || step == 4;
}

if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
    var viewportmeta = document.querySelector('meta[name="viewport"]');
    if (viewportmeta) {
        viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
        document.body.addEventListener('gesturestart', function () {
            viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
        }, false);
    }
}

$(document).ready(function() {
    handleClickingNews();
    setTimeout(function() {
        mql = window.matchMedia("(orientation: portrait)")['matches'];
        setInterval(function() {
            var tmp = window.matchMedia("(orientation: portrait)")['matches'];
            if (mql != null && tmp != mql) {
                //location.reload();
            }
        },1000);

    },300);



    /* fce pro prerozmerovani webu */

    function resize()
    {
        var scroll=step;

        if(carousel==1)
        {
            scroll=step-1;
        }
        width=$(window).width();
        height=$(window).height();

        $('div.page').height(height);
        $('html,body').scrollTop(scroll*height);

    }

    /* fce pro posun referenci */

    function slide(animationSpeed)
    {
        if (animationSpeed === undefined) {
            animationSpeed = 800;
        }
        $('div.slide h3 a').removeClass('active');
        $('div.slide h3 a').eq(carousel).addClass('active');

        $('div.carousel').animate(
        {
            left: -carousel*880 - ($('#r').css('float') == 'none' ? 65 : 0)
        }, animationSpeed);
        window.location.hash = STEPS[step];
    }

    /* fce pro posun okna */

    function scroll(animationSpeed)
    {
        if (animationSpeed === undefined) {
            animationSpeed = 800;
        }
        var scroll=step;
        window.location.hash = STEPS[step];

        $('li.breadcrumb a').html('&#xe600;');
        $('li.breadcrumb a').eq(step).html('&#xe601;');

        if(isWork(step))
        {
            if (step == 2) {
                carousel=0;
            }
            if (step == 3) {
                carousel=1;
            }
            if (step == 4) {
                carousel=2;
            }
            slide(animationSpeed);
        } else {
            carousel = 0;
        }

        /**
         * Prevent scroll down if scrolling through carousel
         */
        if(carousel > 0)
        {
            scroll=step-carousel;
        }

        var scrollTo = (scroll >= 5 ? 3 : scroll) *height;

        $('html,body').animate(
        {
            scrollTop:scrollTo
        }, animationSpeed, function()
        {
            if(step<2 || step>4)
            {
                animation();
            }
        });
    }

    /* animace textu */

    function animation()
    {
        var item=step;

        if(item==5)
        {
            item=3;
        }

        var span=heading.eq(item).find('span');

        if(animated[item]==true || animated[item]==undefined)
        {
            return false;
        }
        else
        {
            animated[item]=true;
        }

        if(item==0)
        {
            var waiting=setInterval(function()
            {
                if(span.eq(0).hasClass('cursor'))
                {
                    span.eq(0).removeClass('cursor');
                }
                else
                {
                    span.eq(0).addClass('cursor');
                }
            },speed*8);

            delay=800;

            setTimeout(function()
            {
                clearInterval(waiting);
            },delay);

            type(span,0,0,1,speed*5);
            type(span,0,1,1,speed*5);
            cursor(span);
        }
        else if (item==1)
        {
            delay=0;
            style(span,1,0,'s',speed*10);
            del(span,1,0,speed*30);
            style(span,1,0,'s',0);
            type(span,1,1,0,speed*5);
            style(span,2,2,'s',speed*5);
            style(span,2,2,'b',speed*5);
            style(span,2,2,'s',speed*5);
            style(span,5,1,'s',speed*5);
            style(span,5,1,'b',speed*5);
            style(span,5,1,'s',speed*5);
            span2=heading.eq(item+1).find('span');
            type(span2,2,0,0,speed);
            style(span2,0,1,'s',speed*5);
            style(span2,0,1,'b',speed*5);
            style(span2,0,1,'s',speed*5);
            style(span,3,1,'s',speed*5);
            cursor(span2,0);
        }
        else if(item==3)
        {
            delay=0;
            style(span,3,1,'s',speed*5);
            style(span,3,1,'s',speed*15);
            style(span,1,1,'s',0);

            setTimeout(function()
            {
                span.eq(1).text(placeholder[3][3]);
                span.eq(3).text(placeholder[3][1]);
            },delay);

            style(span,1,1,'s',speed*10);
            type(span,3,0,1,speed*5);
            type(span,3,4,1,speed);
            type(span,3,5,2,speed*5);
            style(span,5,1,'s',speed*5);
            style(span,5,1,'b',speed*5);
            style(span,5,1,'s',speed*5);
            cursor(span,0);
            setTimeout(function()
            {
                $('form').fadeIn();
            },delay);
        }
    }

    /* vypis textu z htmlka */

    function type(span,step,from,to,wait)
    {
        var position=0;

        delay=delay+wait;

        if(to==0)
        {
            to=headings[step].length;
        }
        else
        {
            to=from+to;
        }

        $.each(headings[step],function(element,string)
        {
            position=element;

            if(element>=from && element<to)
            {
                setTimeout(function()
                {
                    $.each(string.split(''),function(i,letter)
                    {
                        setTimeout(function()
                        {
                            span.eq(element).append(letter);
                        },speed*i);
                    });
                
                    span.removeClass('cursor');
                    span.eq(element).addClass('cursor');
                },delay);

                delay=delay+string.length*speed;
            }
        });

        setTimeout(function()
        {
            span.removeClass('cursor');
        },delay);
    }

    /* mazani textu */

    function del(span,from,to,wait)
    {
        delay=delay+wait;

        if(to==0)
        {
            to=span.length;
        }
        else
        {
            to=from+to;
        }

        setTimeout(function()
        {
            for(i=from;i<to;i++)
            {
                span.eq(i).text('');
            }
        },delay);
    }

    /* stylovani textu */

    function style(span,from,to,style,wait)
    {
        delay=delay+wait;

        if(to==0)
        {
            to=span.length;
        }
        else
        {
            to=from+to;
        }

        setTimeout(function()
        {
            for(i=from;i<to;i++)
            {
                if(span.eq(i).hasClass(style))
                {
                    span.eq(i).removeClass(style);
                }
                else
                {
                    span.eq(i).addClass(style);
                }
            }
        },delay);
    }

    /* animace kurzoru */

    function cursor(span,element)
    {
        if(!element)
        {
            span=span.last();
        }
        else
        {
            span=span.eq(element);
        }

        setTimeout(function()
        {
            setInterval(function()
            {
                if(span.hasClass('cursor'))
                {
                    span.removeClass('cursor');
                }
                else
                {
                    span.addClass('cursor');
                }
            },speed*8);

        },delay);
    }

    /* promenne */

    var pom=0;
    var width;
    var height;
    var mouse=true;
    var step=0;
    var prev=0;
    var carousel=0;
    var delay=0;
    var speed=50;
    var steps=5;
    var heading=$('.about :header, .contact :header');
    var headings=new Array;
    var animated=new Array;
    var placeholder=
        {
            0:
            {
                0:"",
                1:""
            },
            1:
            {
                1:"write ",
                2:"web content, social and video content. ",
                3:"",
                4:"I do intelligent message design and e-mail marketing  and create ideas, concepts and advertising campaigns.",
                5:"",
                6:""
            },
            2:
            {
                0:"",
                1:"",
                2:""
            },
            3:
            {
                0:"",
                1:"growing",
                2:" ",
                3:"business",
                4:"",
                5:"",
                6:""
            }
        };

    heading.map(function(i)
    {
        headings[i]=$(this).find('span').map(function()
        {
            return $(this).text();
        });

        animated[i]=false;

        if(placeholder[i])
        {
            $.each(placeholder[i],function(k,v)
            {
                heading.eq(i).find('span').eq(k).text(v);
            });
        }
    });

    /* resize okna */

    $(window).bind('resize',resize);
    resize();

    /* reset pri nacteni */

    $('div.wrapper').css({'height':'auto'});
    $('div.page').css({'position':'relative'});
    $('div.page, footer').css({'visibility':'visible'});
    $('form').hide();

    /* intro */

    //scroll();

    /* dodelat test aktivniho okna kvuli startu animace :-) */

/*
    var span=$('span[data-text="a"]');
    var text=span.text();
    var start=0;
    var end=text.length;

    span.text('').addClass('cursor');

    setTimeout(function()
    {
        
    },100);
*/
    /*pridani scrollu na kazdou sekci */

    $('section.about div.page, section.references div.page').append('<div class="scroll"><a href="#scroll">Scroll down <span>&#xe602;</span></a></div>');

    //console.log(mouse);

    /* slide referenci */

    $('div.slide h3 a').click(function()
    {
        carousel=$(this).index('div.slide h3 a');

        step=$(this).parents('.page').index('.page')+carousel;

        //console.log('prev: '+prev+' -> step: '+step+' | carousel: '+carousel);
        slide();

        return false;
    });

    /* detail reference */

    $('div.item dl dt a').click(function()
    {
        $('div.item dl dt, div.item dl dd').removeClass('active');
        $(this).parent().addClass('active');
        $(this).parent().next().addClass('active');

        return false;
    });

    /* akce pri kliknuti na scroll */

    $('a[href="#scroll"]').on('click',function()
    {
        step = step+1;
        prev=step-1;
        scroll();
        return false;
    });


    $(window).swipe({
        allowPageScroll: 'none',
        swipeUp: function() {
            if (step < 5) {
                step = step+1;
                prev=step-1;
            }
            scroll();
            return false;
        },
        swipeDown: function() {
            if (step > 0) {
                step = step-1;
                prev=step+1;
            }
            scroll();
            return false;

        }
    })

    /* akce z navigace */

    $('li.breadcrumb a').on('click',function()
    {
        step=$(this).parents('.breadcrumb').index('.breadcrumb');

        if(prev<=step)
        {
            prev=step-1;
        }
        else
        {
            prev=step+1;
        }

        if(step<3)
        {
            carousel=0;
        }

        if ($(this).data('initial')) {
            var animationSpeed = 0;
            $(this).data('initial',false);
        } else {
            var animationSpeed = undefined;
        }
        scroll(animationSpeed);

        return false;
    });

    $('nav li a').not('nav li.breadcrumb a').click(function()
    {
        $(this).parent().find('li.breadcrumb:eq(0) a').trigger('click');

        return false;
    });

    /* akce pro posun koleckem mysi*/

    $(window).mousewheel(function(e)
    {
        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function()
        {
            mouse=true;
            //console.log("true scroll");
        }, 200));

        if(mouse==true)
        {
        pom++;
            if(e.deltaY>0 && e.deltaY<2 && step>0)
            { //scroll up
                prev=step;
                step=step-1;
                scroll();
                mouse=false;
            }
            else if(e.deltaY<0 && e.deltaY>-2 && step<steps)
            { //scroll down
                prev=step;
                step=step+1;
                scroll();
                mouse=false;
            }

            //console.log(prev+' /// '+step);
            //console.log(e.deltaX, e.deltaY, e.deltaFactor);
        //console.log(pom);
        }

        e.preventDefault();
        return false;
    });

    /* externi odkazy v novem okne */

    $('a[rel="external"]').click(function()
    {
        window.open($(this).attr('href'));

        return false;
    });

    /* form - dod�lat */

    $('button').click(function()
    {
        return false;
    });

    /* sipky */

    $(document).keydown(function(e)
    {
        if((e.keyCode==38 || e.keyCode==40) && mouse==true)
        {
            if(e.keyCode==38 && step>0)
            {
                prev=step;
                step=step-1;
                scroll();
                mouse=false;
            }
            else if(e.keyCode==40 && step<steps)
            {
                prev=step;
                step=step+1;
                scroll();
                mouse=false;
            }

            clearTimeout($.data(this, 'scrollTimer'));
            $.data(this, 'scrollTimer', setTimeout(function()
            {
                mouse=true;
                //console.log("sipky");
            }, 800));

            return false;
        }
        //console.log(mouse);
    });

    hardlinks();
    if(step<2 || step>4) {animation();}
    initEmail();
});

function handleClickingNews() {
    $('section.references dl.news ul.dates a').click(function() {
        $('section.references dl.news ul.dates a').removeClass('active');
        $('section.references dl.news div.new').hide();
        $($(this).attr('href')).show();
        $(this).addClass('active');
        return false;
    });
}

function hardlinks() {
    if(window.location.hash) {
        var targetHref = window.location.hash
    } else {
        var targetHref = "";
    }
    $('nav a[href="'+targetHref+'"]').data('initial',true).click();
}

function initEmail() {
    var $form = $('#contact-form');
    var $submit = $form.find('input[type="submit"]');
    $form.submit(function(e) {
        $('#circleG').show();
        $submit.hide();
        $.post(
            $form.attr('action'),
            $form.serialize(),
            function(response) {
                $('#circleG').hide();
                $('#message').show();
                $form.find('input,textarea').val('');
                setTimeout(function() {
                    $('#message').hide();
                    $submit.show();

                },2000);
            }
        )

        e.preventDefault();
    })

}