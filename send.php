<?php
sleep(1);
if ($_POST['email-confirm'] != '') {
    exit; //is bot
}

require 'mailer/PHPMailerAutoload.php';

$email = $_POST['email'];
$name  = $_POST['name'];
$message = $_POST['message'];

$mail = new PHPMailer;

$mail->From = $email;
$mail->FromName = $name;
$mail->addAddress('vojta.u@email.cz', 'Vojta Untermüller');     // Add a recipient
$mail->addReplyTo($email, $name);

$mail->WordWrap = 50;                                 // Set word wrap to 50 characters

$mail->Subject = 'Someone from my website';
$mail->Body    = "
Hi Vojta!

$name ($email) sent you an email with
$message

Have a good day!

";

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
